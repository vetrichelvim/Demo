import React, {Component} from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <span><a href="https://www.ategrity.com/">Ategrity Specialty Insurance Company</a> &copy; 2018 Ategrity Specialty.</span>
        {/* <span className="ml-auto">Powered by <a href="https://genesisui.com">GenesisUI</a></span> */}
      </footer>
    )
  }
}

export default Footer;
