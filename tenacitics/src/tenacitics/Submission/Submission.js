import React, { Component} from 'react';
import { Nav, NavItem, NavLink, ButtonDropdown, 
  Dropdown, 
  DropdownToggle,
   DropdownMenu, 
   DropdownItem, 
   Card,
   CardHeader,
   CardFooter,
   CardBody,
   UncontrolledDropdown, 
   Badge, Button, 
   ButtonGroup,
    Row, Col, 
    TabContent, 
    TabPane, Form, 
    FormGroup, 
    Input } from 'reactstrap';

import classnames from 'classnames';
import '../../../scss/mail.css';

import '../../../scss/brokerage.css';

class Submission extends Component{

    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.dptoggle = this.dptoggle.bind(this);
        this.state = {
          activeTab: '1',
          dropdownOpen: new Array(6).fill(false)
        };
      }
    
      toggle(tab) {
        if (this.state.activeTab !== tab) {
          this.setState({
            activeTab: tab
          });
        }
      }

      dptoggle(i) {
        const newArray = this.state.dropdownOpen.map((element, index) => {
          return (index === i ? !element : false);
        });
        this.setState({
          dropdownOpen: newArray
        });
      }

    render(){      
      var styles = {
        color:'balck',
        backgroundColor:'White',
        // fontWeight:'bold'
        height:'auto'
      };
      var styles1 = {
        color:'balck',
        backgroundColor:'White',
        // fontWeight:'bold'
        height:'auto',        
        borderLeft: '2px solid #f5f5f5',
      };
      return(
            <div className="animated fadeIn">
            <div className="row">
            <div className="col-12 col-md-4" style={styles}>

            <table>
              <tbody>
                <tr>
                  <td><br /></td>
                </tr>
                <tr>
                  <td><Dropdown isOpen={this.state.dropdownOpen[1]} toggle={() => {this.dptoggle(1);}} size="sm">
                  <DropdownToggle caret>
                    Small Dropdown
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem header>Header</DropdownItem>
                    <DropdownItem disabled>Action</DropdownItem>
                    <DropdownItem>Another Action</DropdownItem>
                    <DropdownItem divider/>
                    <DropdownItem>Another Action</DropdownItem>
                  </DropdownMenu>
                </Dropdown></td>
                
                <td>
                <Dropdown isOpen={this.state.dropdownOpen[2]} toggle={() => {this.dptoggle(2);}} size="sm">
                  <DropdownToggle caret>
                    Small Dropdown2
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem header>Header</DropdownItem>
                    <DropdownItem disabled>Action</DropdownItem>
                    <DropdownItem>Another Action</DropdownItem>
                    <DropdownItem divider/>
                    <DropdownItem>Another Action</DropdownItem>
                  </DropdownMenu>
                </Dropdown>
                </td>
                <td>
                  
                <Dropdown isOpen={this.state.dropdownOpen[3]} toggle={() => {this.dptoggle(3);}} size="sm">
                  <DropdownToggle caret>
                    Small Dropdown3
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem header>Header</DropdownItem>
                    <DropdownItem disabled>Action</DropdownItem>
                    <DropdownItem>Another Action</DropdownItem>
                    <DropdownItem divider/>
                    <DropdownItem>Another Action</DropdownItem>
                  </DropdownMenu>
                </Dropdown>
                
                 </td>
                </tr>
                <tr>
                  <td><br /></td>
                </tr>
                <tr>
                  <td colSpan="3" align="left">
                  <input type="text" id="Search" name="Search" placeholder="Search" className="form-control" /></td>
                </tr>
                </tbody>
            </table>
            <br />
           	<section className="content">
			{/* <h1>Table Filter</h1> */}
			<div className="col-md-12 col-md-offset-2">
				<div className="panel panel-default">
					<div className="panel-body">
						{/* <div className="pull-right">
							<div className="btn-group">
								<button type="button" className="btn btn-success btn-filter" data-target="pagado">Pagado</button>
								<button type="button" className="btn btn-warning btn-filter" data-target="pendiente">Pendiente</button>
								<button type="button" className="btn btn-danger btn-filter" data-target="cancelado">Cancelado</button>
								<button type="button" className="btn btn-default btn-filter" data-target="all">Todos</button>
							</div>
            </div> */}
          
						<div className="table-container">
							<table className="table table-filter">
								<tbody>
									<tr data-status="pagado">
										<td>
											<div className="ckbox">
												<input type="checkbox" id="checkbox1" />
												<label></label>
											</div>
										</td>
										{/* <td>
											<a href="javascript:;" className="star">
												<i className="glyphicon glyphicon-star"></i>
											</a>
                    </td> */}
                    <td></td>
										<td>
											<div className="media">
												<a href="#" className="pull-left">
													{/* <img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg" className="media-photo" /> */}
												</a>
												<div className="media-body">
													<span className="media-meta pull-right">Febrero 13, 2016</span>
													<h4 className="title">
														Lorem Impsum
														{/* <span className="pull-right pagado">(Pagado)</span> */}
													</h4>
													<p className="summary">Ut enim ad minim veniam, quis nostrud exercitation...</p>
												</div>
											</div>
										</td>
									</tr>
									<tr data-status="pendiente">
										<td>
											<div className="ckbox">
												<input type="checkbox" id="checkbox3" />
												<label></label>
											</div>
										</td>
										<td>
											<a href="javascript:;" className="star">
												<i className="glyphicon glyphicon-star"></i>
											</a>
										</td>
										<td>
											<div className="media">
												<a href="#" className="pull-left">
													{/* <img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg" className="media-photo" /> */}
												</a>
												<div className="media-body">
													<span className="media-meta pull-right">Febrero 13, 2016</span>
													<h4 className="title">
														Lorem Impsum
														{/* <span className="pull-right pendiente">(Pendiente)</span> */}
													</h4>
													<p className="summary">Ut enim ad minim veniam, quis nostrud exercitation...</p>
												</div>
											</div>
										</td>
									</tr>
									<tr data-status="cancelado">
										<td>
											<div className="ckbox">
												<input type="checkbox" id="checkbox2" />
												<label></label>
											</div>
										</td>
										<td>
											<a href="javascript:;" className="star">
												<i className="glyphicon glyphicon-star"></i>
											</a>
										</td>
										<td>
											<div className="media">
												<a href="#" className="pull-left">
													{/* <img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg" className="media-photo" /> */}
												</a>
												<div className="media-body">
													<span className="media-meta pull-right">Febrero 13, 2016</span>
													<h4 className="title">
														Lorem Impsum
														{/* <span className="pull-right cancelado">(Cancelado)</span> */}
													</h4>
													<p className="summary">Ut enim ad minim veniam, quis nostrud exercitation...</p>
												</div>
											</div>
										</td>
									</tr>
									<tr data-status="pagado" className="selected">
										<td>
											<div className="ckbox">
												<input type="checkbox" id="checkbox4" checked />
												<label></label>
											</div>
										</td>
										<td>
											<a href="javascript:;" className="star star-checked">
												<i className="glyphicon glyphicon-star"></i>
											</a>
										</td>
										<td>
											<div className="media">
												<a href="#" className="pull-left">
													{/* <img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg" className="media-photo" /> */}
												</a>
												<div className="media-body">
													<span className="media-meta pull-right">Febrero 13, 2016</span>
													<h4 className="title">
														Lorem Impsum
														{/* <span className="pull-right pagado">(Pagado)</span> */}
													</h4>
													<p className="summary">Ut enim ad minim veniam, quis nostrud exercitation...</p>
												</div>
											</div>
										</td>
									</tr>
									<tr data-status="pendiente">
										<td>
											<div className="ckbox">
												<input type="checkbox" id="checkbox5" />
												<label></label>
											</div>
										</td>
										<td>
											<a href="javascript:;" className="star">
												<i className="glyphicon glyphicon-star"></i>
											</a>
										</td>
										<td>
											<div className="media">
												<a href="#" className="pull-left">
													{/* <img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg" className="media-photo" /> */}
												</a>
												<div className="media-body">
													<span className="media-meta pull-right">Febrero 13, 2016</span>
													<h4 className="title">
														Lorem Impsum
														{/* <span className="pull-right pendiente">(Pendiente)</span> */}
													</h4>
													<p className="summary">Ut enim ad minim veniam, quis nostrud exercitation...</p>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				
			</div>
		</section>

            </div>
            
            
            <div className="col-12 col-md-8" style={styles1}>
            <Col xs="12" md="12" className="mb-4">
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '1' })}
                  onClick={() => { this.toggle('1'); }}
                >
                  <i className="icon-calculator"></i> <span className={ this.state.activeTab === '1' ? "" : "d-none"}> Quote</span>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '2' })}
                  onClick={() => { this.toggle('2'); }}
                >
                  <i className="fa fa-envelope fa-1"></i> <span
                  className={ this.state.activeTab === '2' ? "" : "d-none"}> Mail</span>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '3' })}
                  onClick={() => { this.toggle('3'); }}
                >
                  <i className="fa fa-paperclip fa-lg"></i> <span className={ this.state.activeTab === '3' ? "" : "d-none"}> Attachment</span>
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={this.state.activeTab}>


              <TabPane tabId="1">

            <div className="container">
          <div className="row">
          <div className="col-md-12 bgwhite wrapper">
                 <Card>
              <CardHeader>
                <strong>Quote Form</strong>
              </CardHeader>
              <CardBody>
                    <form>
                        <div className="col-md-12 ">
                       
                        <div className="form-group col-md-6">
                            <label for="exampleFormControlInput1" className="text-left">CC</label>
                            <input type="text" className="form-control" id="exampleFormControlInput1" placeholder="Enter CC"/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="exampleFormControlInput1" className="text-left">ZIP</label>
                            <input type="text" className="form-control" id="exampleFormControlInput1" placeholder="Enter Zip"/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="exampleFormControlInput1" className="text-left">TC</label>
                            <input type="text" className="form-control" id="exampleFormControlInput1" placeholder="Enter TC"/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="exampleFormControlSelect1" className="text-left">Premium Basis</label>
                            {/* <select class="form-control" id="exampleFormControlSelect1">
                            <option selected="true" disabled="disabled">Select</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            </select> */}

                             <Input type="select" name="select" id="select">
                            <option value="">Please select</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            </Input>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="exampleFormControlInput1" className="text-left">Sales</label>
                            <input type="text" className="form-control" id="exampleFormControlInput1" placeholder="Sales"/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="exampleFormControlInput1" className="text-left">Payroll</label>
                            <input type="text" className="form-control" id="exampleFormControlInput1" placeholder="Payroll"/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="exampleFormControlInput1" className="text-left">Average</label>
                            <input type="text" className="form-control" id="exampleFormControlInput1" placeholder="Average"/>
                        </div>
                       
                        <div className="clearfix"></div>   
                        <br /> 
                      
                        <div className="form-group col-md-12">
                            <label for="exampleFormControlInput1" className="text-left col-md-12">Coverage</label>
                            <div className="col-md-12">
                            <label className="radio-inline col-md-3"><input type="radio" name="optradio" />Option 1</label>
                            <label className="radio-inline col-md-3"><input type="radio" name="optradio" />Option 2</label>
                            <label className="radio-inline col-md-3"><input type="radio" name="optradio" />Option 3</label>
                        </div>
                        </div>

                         <div className="form-group col-md-12">
                            <label for="exampleFormControlInput1" className="text-left col-md-12">Lists</label>
                            <div className="col-md-12">
                            <label className="radio-inline col-md-2"><input type="radio" name="optradio" />Option 1</label>
                            <label className="radio-inline col-md-2"><input type="radio" name="optradio" />Option 2</label>
                          
                        </div>
                        </div>


                         <div className="form-group col-md-12">
                            <label for="exampleFormControlInput1" className="text-left col-md-12">Deductibles</label>
                            <div className="col-md-12">
                            <label className="radio-inline col-md-2"><input type="radio" name="optradio" />Option 1</label>
                            <label className="radio-inline col-md-2"><input type="radio" name="optradio" />Option 2</label>
                            <label className="radio-inline col-md-2"><input type="radio" name="optradio" />Option 3</label>
                            <label className="radio-inline col-md-2"><input type="radio" name="optradio" />Option 4</label>
                        </div>
                        </div>


                        <div className="form-group col-md-6">
                            <label for="exampleFormControlInput1" className="text-left">Effective Date</label>
                            <input type="date" className="form-control" id="exampleFormControlInput1" placeholder=""/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="exampleFormControlInput1" className="text-left">Exposive</label>
                            <input type="text" className="form-control" id="exampleFormControlInput1" placeholder="Exposive"/>
                        </div>
                    
                        <div className="clearfix"></div>   
                        <br />  
                        <div className="form-group col-md-6">
                            <label for="exampleFormControlInput1" className="text-left">Rate</label>
                            <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Enter Rate"/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="exampleFormControlInput1" className="text-left">Rem</label>
                            <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Exposive*Rate"/>
                        </div>
                        <div className="form-group col-md-6">
                            <label for="exampleFormControlInput1" className="text-left">Composite Rate</label>
                            <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Total Sales/Tot Rem"/>
                        </div>
                        </div>
                        </form>

                         </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button>
              </CardFooter>
            </Card>
            </div>
            </div>
            </div>


              </TabPane>







              <TabPane tabId="2">
              
            <div className="email-app mb-4">
            
          <main className="message">
              <div className="details">
              {/* <div className="title">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.</div> */}
              <div className="header">
                <img className="avatar" src="img/avatars/7.jpg"/>
                <div className="from">
                  <span>Lukasz Holeczek</span>
                  lukasz@bootstrapmaster.com
                </div>
                <div className="date">Today, <b>3:47 PM</b></div>
              </div>
              <div className="content">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <blockquote>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </blockquote>
              </div>
              
              {/* <form method="post" action="">
                <FormGroup>
                  <Input type="textarea" id="message" name="body" rows="12" placeholder="Click here to reply"></Input>
                </FormGroup>
                <FormGroup>
                  <Button type="submit" color="success">Send message</Button>
                </FormGroup>
              </form> */}
            </div>
            </main>
            </div>
              </TabPane>
              <TabPane tabId="3">
              <div className="attachments">
                <div className="attachment">

                <span>Zip</span>
                <hr /> 
                <i class="fa fa-file-zip-o fa-lg mt-4" style={{color:'#ff5454'}}></i> <b>bootstrap.zip</b> 

                  {/* <Badge color="danger">zip</Badge> <b>bootstrap.zip</b> */}
                   {/* <i>(2,5MB)</i> */}
                  {/* <span className="menu">
                    <a href="#" className="fa fa-search"></a>
                    <a href="#" className="fa fa-share"></a>
                    <a href="#" className="fa fa-cloud-download"></a>
                  </span> */}
                </div>
                <div className="attachment">
                <span>Text</span>
                <hr />

                 <i class="fa fa-file-text fa-lg mt-4" style={{color:'#67c2ef'}}></i> <b>readme.txt</b> 
                  {/* <Badge color="info">txt</Badge> <b>readme.txt</b> */}
                   {/* <i>(7KB)</i>
                  <span className="menu">
                    <a href="#" className="fa fa-search"></a>
                    <a href="#" className="fa fa-share"></a>
                    <a href="#" className="fa fa-cloud-download"></a>
                  </span> */}
                </div>
                <div className="attachment">
                <span>Excel</span>
                <hr />
                  <i class="fa fa-file-excel-o fa-lg mt-4" style={{color:'#FF0000'}}></i> <b>spreadsheet.xls</b>
                   {/* <i>(984KB)</i>
                  <span className="menu">
                    <a href="#" className="fa fa-search"></a>
                    <a href="#" className="fa fa-share"></a>
                    <a href="#" className="fa fa-cloud-download"></a>
                  </span> */}
                </div>
              </div>
              </TabPane>
            </TabContent>
          </Col>
            </div>


            </div>
            </div>
        )
    }
} 

export default Submission;