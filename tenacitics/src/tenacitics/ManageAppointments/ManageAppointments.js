import React, {Component} from 'react';
import {Badge,Row,Col,Card,CardHeader,CardBody,Table,Pagination,PaginationItem,PaginationLink} from 'reactstrap';

import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import axios from 'axios';

import 'spinkit/css/spinkit.css';

class ManageAppointments extends Component {
    constructor(props){
        super(props);
        this.state = {
            data:[],
            jsonFromDatabase: [],
        }
        this.cellButton = this.cellButton.bind(this);
        this.onClickGroupToUpdate = this.onClickGroupToUpdate.bind(this);
    }
    componentDidMount(){
        var rows = [];
        let currentComponent = this;
            fetch('http://localhost/wbui/views/crmGetLeads_plain.php')
            .then(function(response) {
                if (response.status >= 400) {
                throw new Error("Bad response from server");
                }
            return response.json();
            })
            .then(function(res) {
               // console.log(res.data);
            //that.setState({ person: data.person });
            currentComponent.setState({ data : res.data});
        });
        // .then((data) => {
        //     const jsonData = JSON.parse(JSON.stringify(data));
        //    // console.log(jsonData);
        //     // rows.push(jsonData);
        //     this.setState(prevState => ({data: [jsonData]}))
        // })
        // this.setState({
        //     data:rows
        // });  

        // function showmethod(phpurl,email,fullname){
        //     console.log(phpurl);
        // }
       
    }

    cellButton(cell, row, enumObject, rowIndex) {
        let theButton;
        let groupExistsInDatabase = false;
        for(var group in this.state.jsonFromDatabase){
          if (this.state.jsonFromDatabase[group].id === row.id){
            // make groupExistsInDatabase true if the group is found in the database
            groupExistsInDatabase = true;
            break;
          }
        }

        theButton = <button  className="btn btn-primary btn-xs" type="button" 
        onClick={() => this.onClickGroupToUpdate(cell, row, rowIndex)}>
                         Send Email
                       </button>
      
        // if(groupExistsInDatabase === true) {
        //   theButton = <button style={{ backgroundColor: "red" }} type="button" onClick={() => this.onClickGroupToUpdate(cell, row, rowIndex)}>
        //                 Update the group
        //               </button>
        // } else {
        //   theButton = <button style={{ backgroundColor: "blue" }} type="button" onClick={() => this.onClickGroupSelected(cell, row, rowIndex)}>
        //                 Process the group
        //               </button>
        // }
        return theButton;
      }

      onClickGroupToUpdate(cell, row, rowIndex){
          var fullname = row[0]+' '+row[1];
          var brokeremail = row[2];
          var siteuri = "http://ategrity-dev.esinsurancecloud.com/wbui/v1/index.php/invitebroker";
          //var siteuri = "http://localhost/wbui/v1/index.php/invitebroker";
         //var siteuri = "http://localhost/wbui/reactjs_test.php"

        //   return fetch(siteuri,{
        //         method: 'POST',
        //       //  mode: 'CORS',
        //         // headers: {
        //         // Accept: 'application/json',
        //         // 'Content-Type': 'application/json',
        //         // },
        //        // body:{email:email,fullname:fullname},
        //        body:{email:brokeremail,fullname:fullname},
        //     //    body: JSON.stringify({
        //     //     email: brokeremail,
        //     //     fullname:fullname,
        //     //   }),
        //   }).then(response => {
        //     // if (response.status >= 200 && response.status < 300) {
        //     //     return response;
        //     //     console.log(response);
        //     //     window.location.reload();
        //     //   } else {
        //     //    console.log('Somthing happened wrong');
        //     //   }
        //     console.log(response);
        // }).catch(err => err);
        //    // console.log(err);
        // // var datas = {
        // //     email:email,
        // //     fullname: fullname
        // //   }
        var bodyFormData = new FormData();
        bodyFormData.set('email', brokeremail);
        bodyFormData.set('fullname', fullname);

        axios({
            method: 'POST',
            url: siteuri,
            // headers: {
            //     //              'Content-Type': 'application/json'
            //           'Content-Type': 'multipart/form-data'
            //     //'Content-Type': 'text/plain'
            //                 },
            // data: {
            //     "email":brokeremail,"fullname":fullname
            // }, 
            data:bodyFormData,
            config: { headers: {'Content-Type': 'multipart/form-data' }}

          }).then(function(response) {
            console.log(response.data);
            var msg = response.data;
            if(msg['error'] == "0") {
                alert ("Broker " +brokeremail+ " has been invited to signup");
                //location.reload();
                }
              else {
                  alert ('Unable to send an invitation to the broker');
              }
          });

      }

    render(){

        this.table = this.state.data;
        this.options = {
        sortIndicator: true,
        hideSizePerPage: true,
        paginationSize: 2,
        //sizePerPage: 5,
        hidePageListOnlyOnePage: true,
        clearSearch: true,
        alwaysShowAllBtns: false,
        withFirstAndLast: false,
        noDataText: (<div className="sk-fading-circle"><div className="sk-spinner sk-spinner-pulse"></div></div>)
        }
     // console.log(this.state.data);
    //   function buttonFormatter(cell, row){
    //     return '<BootstrapButton type="submit" class="btn btn-primary btn-xs" onClick={this.buttonClicked}>Send Email</BootstrapButton>';
    //   }

    //   function buttonClicked() {
    //     console.log('Button was clicked!')
    // }
        return (
           <div>
               

            {/* <Table responsive>
            <thead>
            <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Action</th>
            </tr>
            </thead>
                    <tbody>
                    {this.state.data.map(function(dynamicData) {
             
             return (
                <tr>
                   
                    <td>{dynamicData}</td>
                    <td></td>
                </tr>
              )
           
           })}
                  </tbody>
            </Table> */}
          

              <BootstrapTable data={this.table} version="4" striped hover pagination search options={this.options}>
              <TableHeaderColumn  dataField="0" dataSort>First Name</TableHeaderColumn>
              <TableHeaderColumn isKey dataField="1">Last Name</TableHeaderColumn>
              <TableHeaderColumn dataField="2" dataSort>Email</TableHeaderColumn>
              <TableHeaderColumn dataField="3" dataSort>Status</TableHeaderColumn>
              <TableHeaderColumn dataField="button" dataFormat={this.cellButton} dataSort>Action</TableHeaderColumn>

            </BootstrapTable>
           </div>    
        )
    }
} 

export default ManageAppointments;