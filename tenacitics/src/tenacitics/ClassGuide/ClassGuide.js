import React, {Component} from 'react';
import {Badge,Row,Col,Card,CardHeader,CardBody,Table,Pagination,PaginationItem,PaginationLink} from 'reactstrap';

import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import axios from 'axios';
import '../../../scss/class_guide_style.css';

//import map from '../../../public/img/world-map-icon.png';

//import my_image from './img/world-map-icon.png';

class ClassGuide extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div>
                
<div className="container">
  <div className="row">
	<div className="col-md-12 wrapper">
		{/* <h4 className="heading"> <span className="glyphicon glyphicon-th"></span> PRODUCTS</h4> */}
		<div className="lists">
			<ul>
				<li><a href="">General Liability</a></li>
				<li><a href="">Property</a></li>
				<li><a href="">Excess Liability</a></li>
				<li><a href="">Inland Marine</a></li>
				<li><a href="">Liquor Liability</a></li>
				<li><a href="">OCP</a></li>
				<li><a href="">Special Event</a></li>
			</ul>
			
			<form className="classcode" action="">
		  <div className="form-group col-md-7 p0 mr20">
			<label for="classcode">Keyword or Classcode</label>
			<input type="text" className="form-control" id="" placeholder="Enter keyword or classcode" />
		  </div>
		  <div className="form-group col-md-4 pr0">
			<label for="zip">Zip Code</label>
			<input type="text" className="form-control" id="" placeholder="Zip code" />
		  </div>
		
		</form>
		<div className="details">
		<h5>Class Details</h5>
        {/* <img src='public/img/world-map-icon.png'  className="location" alt="location" /> */}
        {/* <img src={require('../img/world-map-icon.png')}  className="location" alt="location" /> */}
        {/* <img src={my_image}  className="location" alt="location" /> */}
        <i className="fa fa-map-signs fa-lg mt-4 location" style={{ color: "red" }}></i>
		<p> NJ - Territory: 511, Long Branch and Vicinity ZIP codes (08742)</p>
		</div>
		</div>
		
		<div className="note">
		<span className="glyphicon glyphicon-pencil"></span>
			<p><span className="txt">Notes: </span> This Classification applies to buildings which are entirely vacant. If the building is being renovated, add the appropriate subcontracting code and rate per 1,000 construction costs.</p>
		</div>
		
		<div className="authority">
			<h5>Authority</h5>
			<ul>
				<li>
					<span className="glyphicon glyphicon-thumbs-up"></span>
					<p><strong>PremOps</strong><br /> <span>Agent Authority</span></p>
					
				</li>
				<li>
					 <span className="glyphicon glyphicon-thumbs-down"></span>
					<p><strong>ProdCOps</strong><br /> <span>Prohibited</span></p>
					
				</li>
			</ul>
			
		</div>
		
        
		<div className="authority">
				<h5>Exposure Data</h5>
            
            <div className="row">
            <div className="col-md-2">
                <div className="form-group">
                    <label for="street">Exposure</label>
                    <input type="text" name="Exposure" className="form-control" id="Exposure"
                     placeholder="" value="" />
                </div>
            </div>
            <div className="col-md-2">
                <div className="form-group">
                    <label for="street">Effective Date</label>
                    <input type="text" name="effective_from" className="form-control" id="effective_from" 
                    placeholder="" />
                </div>
            </div>
            </div>


<div className="row">
        <div className="col-md-9 col-form-label">
        <div className="form-group ">


        <label for="street">Coverages</label>
        <div className="form-check form-check-inline mr-1">
        <input className="form-check-input" type="checkbox" id="inline-checkbox1" value="check1" checked="checked" />
        <label className="form-check-label" for="inline-checkbox1">PremOps</label>
        </div>
        <div className="form-check form-check-inline mr-1">
        <input className="form-check-input" type="checkbox" id="inline-checkbox2" value="check2" checked="checked" />
        <label className="form-check-label" for="inline-checkbox2">ProdCops</label>
        </div>
        <div className="form-check form-check-inline mr-1">
        <input className="form-check-input" type="checkbox" id="inline-checkbox3" value="check3" />
        <label className="form-check-label" for="inline-checkbox3">Liquor Liablity</label>
        </div>

        </div>
        </div>
</div>

<div className="row">
<div className="col-md-4">
<div className="form-group ">
<label for="street">Limits</label>
<div className="form-check form-check-inline mr-1">
<input className="form-check-input limits" type="radio" value="1221" name="inline-radios1" checked="checked" />
<label className="form-check-label" for="inline-radio1">1/2/2/1</label>
</div>
<div className="form-check form-check-inline mr-1">
<input className="form-check-input limits" type="radio" id="inline-radio2" value="2442" name="inline-radios1" />
<label className="form-check-label" for="inline-radio2">2/4/4/1</label>
</div>

</div>
</div>

<div className="col-md-8">
<div className="form-group ">
<label for="street">Deductibles</label>
<div className="form-check form-check-inline mr-1">
<input className="form-check-input" type="radio" id="inline-radio1" value="option1" name="inline-radios" checked="checked" />
<label className="form-check-label" for="inline-radio1">2500</label>
</div>
<div className="form-check form-check-inline mr-1">
<input className="form-check-input" type="radio" id="inline-radio2" value="option2" name="inline-radios" />
<label className="form-check-label" for="inline-radio2">5000</label>
</div>
<div className="form-check form-check-inline mr-1">
<input className="form-check-input" type="radio" id="inline-radio2" value="option2" name="inline-radios" />
<label className="form-check-label" for="inline-radio2">10000</label>
</div>
<div className="form-check form-check-inline mr-1">
<input className="form-check-input" type="radio" id="inline-radio2" value="option2" name="inline-radios" />
<label className="form-check-label" for="inline-radio2">25000</label>
</div>

</div>
</div>
</div>


</div>

		
                <div className="rate">
                    <h5>Rates & Premiums</h5>
                                <table className="table table-bordered table-hover">
                            <thead>
                            <tr>
                            <th>Description</th>
                            <th>$300/$300</th>
                            <th>$300/$600</th>
                            <th>$500/$500</th>
                            <th>$500/$1,000</th>
                            <th>$1,000/$1,000</th>
                            <th>$1,000/$2,000</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr className="success">
                            <td>Minimum Premium</td>
                            <td>$325</td>
                            <td>$350</td>
                            <td>$375</td>
                            <td>$400</td>
                            <td>$450</td>
                            <td>$500</td>
                            </tr>      
                            <tr className="warning">
                            <td>511</td>
                            <td>$44.31</td>
                            <td>$45.66</td>
                            <td>$51.07</td>
                            <td>$52.09</td>
                            <td>$60.21</td>
                            <td>$60.88</td>
                            </tr>

                            </tbody>
                            </table>
                </div>
                <br />
    <div className="row">
    <div className="col-md-12 text-center">
    <button type="submit" className="btn btn-sm btn-primary get_quote_api"><i className="fa fa-dot-circle-o"></i> Get Indicative Quote</button>
    &nbsp;&nbsp;&nbsp;<button type="reset" className="btn btn-sm btn-danger"><i className="fa fa-ban"></i> Clear all</button>
    </div>
    </div>
    <br/>
    <br/>

	</div>	
		
  </div>
</div>
            </div>
        )
    }

}

export default ClassGuide;